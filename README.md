	
		
# Bachelorarbeit

Das Git-Repository besteht aus den Ordnern src und data. In Data befindet sich der Datensatz für Test und Training der Modelle.
In  src befindet sich drei Python-Datein:

1. main.py

2. functions.py

3. hyperparameteroptimization.py

In main.py werden die daten verarbeitet und die Modelle trainiert.
In functions.py befinden sich alle Hilfsfunktionen.
In hyperparameteroptimization.py wurden die Hyperparameter der Modelle optimiert und manuell nach main.py übertragen.
## Installation

Download and install [Python 3](https://www.python.org/downloads/)

Download [pip](https://pip.pypa.io/en/stable/) 

Download [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

Add python and pip to [path](https://docs.telerik.com/teststudio/features/test-runners/add-path-environment-variables) 

```bash
pip install virtualenv 
```

Update  virtualenv


```bash
pip --upgrade virtualenv
```

create virtual environment

```bash
virtualenv venv 
```

## Run
activate virtual environment (Windows)
```bash
.\venv\Scripts\activate
```
If an error occurs:

1. open Windowsshell as administrator

2. Execute following:

```bash
 Get-Executionpolicy -Executionpolicy Bypass -Scope LocalMachine
```

clone git repository
```bash
git clone https://fatihozgan@bitbucket.org/fatihozgan/bachelorarbeit.git
```

open folder

```bash
cd bachelorarbeit
```

install packages

```bash
pip install -r requirements
```

run main.py

```bash
python .\src\main.py
```
