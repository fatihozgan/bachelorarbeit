import numpy as np
import pandas as pd
from sklearn.datasets import make_multilabel_classification
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import (accuracy_score, classification_report,
                             confusion_matrix, f1_score)
from sklearn.model_selection import (GridSearchCV, RandomizedSearchCV,
                                     train_test_split)
from sklearn.naive_bayes import GaussianNB, MultinomialNB
from skmultilearn.problem_transform import BinaryRelevance

from functions import binarization, data_pipeline, modelfit, readexcelfile



# Classifier
random = 2
rf_grid_title = RandomForestClassifier(n_estimators=2000, min_samples_split=7,
                                       min_samples_leaf=1, max_features="sqrt",
                                       max_depth=70, bootstrap=False, random_state=random, n_jobs=-1)

rf_grid_summary = RandomForestClassifier(n_estimators=1300, min_samples_split=2,
                                         min_samples_leaf=1, max_features="sqrt",
                                         max_depth=100, bootstrap=False, random_state=random, n_jobs=-1)

rf_grid_content = RandomForestClassifier(n_estimators=1100, min_samples_split=5,
                                         min_samples_leaf=1, max_features="sqrt",
                                         max_depth=100, bootstrap=False, random_state=random, n_jobs=-1)

lr_grid_title = LogisticRegression(penalty="l2", C=10, solver='liblinear', random_state=random)

lr_grid_summary = LogisticRegression(
    penalty="l2", C=10000, solver='liblinear', random_state=random)

lr_grid_content = LogisticRegression(penalty="l2", C=10, solver='liblinear', random_state=random)

mnb = MultinomialNB(alpha=1, fit_prior=True)

# list of Classifier and names

model_title = [rf_grid_title, lr_grid_title, mnb]
model_summary = [rf_grid_summary, lr_grid_summary, mnb]
model_content = [rf_grid_content,  lr_grid_summary, mnb]
model_names = ['rf', 'lr', 'mnb']

model_title_stage2 = [rf_grid_title, BinaryRelevance(
    lr_grid_title), BinaryRelevance(mnb)]

model_summary_stage2 = [rf_grid_summary, BinaryRelevance(
    lr_grid_summary), BinaryRelevance(mnb)]

model_content_stage2 = [rf_grid_content, BinaryRelevance(
    lr_grid_content), BinaryRelevance(mnb)]

# list of Classifier for Ensemble Learning

estimators_title = [("rf", rf_grid_title),
                    ("lr", lr_grid_title), ("mnb", mnb)]
estimators_summary = [("rf", rf_grid_summary),
                      ("lr", lr_grid_summary), ("mnb", mnb)]
estimators_content = [("rf", rf_grid_content),
                      ("lr", lr_grid_content), ("mnb", mnb)]

#############################################################################
####################-------Preprocessing-------##############################
#############################################################################

df = readexcelfile(
    ".\\Data\\datensatz.xlsx", "datensatz")

print("preprocessing data for stage 1..")
title_tfidf = data_pipeline(df, "title")
summary_tfidf = data_pipeline(df, "summary")
content_tfidf = data_pipeline(df, "content")

print("transform targets in binary values")
binarization(df, "category", "energy")
binarization(df, "r&d/technology", "yes")
binarization(df, "acquisition/merger/joint venture/framework agreement", "yes")
binarization(df, "project/investment", "yes")

y = df["category"].astype("int")
df_stage2 = df[df.category == 1]


# labels
y_stage2 = df_stage2.loc[:, [
    "r&d/technology", "acquisition/merger/joint venture/framework agreement", "project/investment"]]
y_stage2 = np.array(y_stage2)
y_stage2 = y_stage2.astype("int")

print("preprocess data for stage 2")
title_tfidf_stage2 = data_pipeline(df_stage2, "title")
summary_tfidf_stage2 = data_pipeline(df_stage2, "summary")
content_tfidf_stage2 = data_pipeline(df_stage2, "content")

######################################################################
##################---------Train-Fit-Stage1-----------------##########
######################################################################

results_info="'model name': (Accuracy, F1-Score, Confusionmatrix)"
results_title = modelfit(title_tfidf, y, model_title,
                         model_names, estimators_title)
print("\nResults for title Stage 1")
print(results_info)
print(results_title)


results_summary = modelfit(
    summary_tfidf, y, model_summary, model_names, estimators_summary)
print("\nResults for Summary Stage 1")
print(results_info)
print(results_summary)


results_content = modelfit(
    content_tfidf, y, model_content, model_names, estimators_content)
print("\nResults for Content Stage 1")
print(results_info)
print(results_content)


######################################################################
##################---------Train-Fit-Stage2-----------------##########
######################################################################


model_names_stage2 = ['rf', 'BR_lr', 'BR_mnb']


results_title_stage2 = modelfit(
    title_tfidf_stage2, y_stage2, model_title_stage2, model_names_stage2)
print("\nResults for title Stage 2")
print(results_info)
print(results_title_stage2)


results_summary_stage2 = modelfit(
    summary_tfidf_stage2, y_stage2, model_summary_stage2, model_names_stage2)
print("\nResults for Summary Stage 2")
print(results_info)
print(results_summary_stage2)


results_content_stage2 = modelfit(
    content_tfidf_stage2, y_stage2, model_content_stage2, model_names_stage2)
print("\nResults for Content Stage 2")
print(results_info)
print(results_content_stage2)
