from CrossValidation import title_tfidf, content_tfidf, summary_tfidf, y
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split, RandomizedSearchCV, GridSearchCV
from sklearn.naive_bayes import MultinomialNB


# hier werden für title, summary und content train test split durchgeführt
X_title_train, X_title_test, y_title_train, y_title_test = train_test_split(title_tfidf, y,
                                                                            test_size=0.2, random_state=1)
X_content_train, X_content_test, y_content_train, y_content_test = train_test_split(content_tfidf, y,
                                                                                    test_size=0.2, random_state=1)
X_summary_train, X_summary_test, y_summary_train, y_summary_test = train_test_split(summary_tfidf, y,
                                                                                    test_size=0.2, random_state=1)

rf = RandomForestClassifier()
mnb = MultinomialNB()
lr = LogisticRegression()

# MultinomialNaiveBayes

mnb_param = {"alpha": [0.1, 1, 10],
             "fit_prior": [True, False]}

# anwenden von GridsearchCV für MNB 
#title
mnb_grid_title = GridSearchCV(
    estimator=mnb, param_grid=mnb_param, cv=5, n_jobs=-1, verbose=2)
mnb_grid_title.fit(X_title_train, y_title_train)
y_pred = mnb_grid_title.predict(X_title_test)
print(mnb_grid_title.best_params_)
print("MNB TITLE:\", accuracy_score(y_title_test, y_pred))

#summary
mnb_grid_summary = GridSearchCV(
    estimator=mnb, param_grid=mnb_param, cv=5, n_jobs=-1, verbose=2)
mnb_grid_summary.fit(X_summary_train, y_summary_train)
y_pred = mnb_grid_summary.predict(X_summary_test)
print(mnb_grid_summary.best_params_)

#content
mnb_grid_content = GridSearchCV(
    estimator=mnb, param_grid=mnb_param, cv=5, n_jobs=-1, verbose=2)
mnb_grid_content.fit(X_content_train, y_content_train)
y_pred = mnb_grid_content.predict(X_content_test)
print(mnb_grid_content.best_params_)

"""
Ergebnisse mnb alpha:1, fit_prior = True
"""

###########################################################################
#######################----LogisticRegression----##########################
###########################################################################



# Anwenden von RandomizedSearchCV für  Logistic Regresion


random_grid_lr = {'C': [0.0001, 0.001, 0.1, 1, 10, 100, 1000, 10000],
                  'penalty': ['l1', 'l2']}

# title
lr_random_title = RandomizedSearchCV(estimator=lr, param_distributions=random_grid_lr,
                                     cv=5, n_iter=100, verbose=2, random_state=42, n_jobs=-1)
lr_random_title.fit(X_title_train, y_title_train)
print(lr_random_title.best_params_)

#summary
lr_random_summary = RandomizedSearchCV(estimator=lr, param_distributions=random_grid_lr,
                                       cv=5, n_iter=100, verbose=2, random_state=42, n_jobs=-1)
lr_random_summary.fit(X_summary_train, y_summary_train)
print(lr_random_summary.best_params_)

#content
lr_random_content = RandomizedSearchCV(estimator=lr, param_distributions=random_grid_lr,
                                       cv=5, n_iter=100, verbose=2, random_state=42, n_jobs=-1)
lr_random_content.fit(X_content_train, y_content_train)
print(lr_random_content.best_params_)

"""
Ergebnisse RandomizedSearchCV:
 
title_param_lr= {'penalty': 'l2', 'C': 10}
summary_param_lr={'penalty': 'l2', 'C': 10000}
content_param_lr={'penalty': 'l2', 'C': 10}
"""
# create grid_params from RandomSearchCV Results
title_grid_param_lr = {'penalty': ['l2'],
                       'C': [1, 10, 100]}

summary_grid_param_lr = {'penalty': ['l2'],
                         'C': [10, 100, 1000, 10000]}

content_grid_param_lr = {'penalty': ['l2'],
                         'C': [1, 10, 100]}
# anwenden von GridSearchCV auf LR
# title
lr_grid_title = GridSearchCV(lr, param_grid=title_grid_param_lr, cv=5,
                             n_jobs=-1, verbose=2)
lr_grid_title.fit(X_title_train, y_title_train)
print(lr_grid_title.best_params_)
# summary
lr_grid_summary = GridSearchCV(lr, param_grid=summary_grid_param_lr, cv=5,
                               n_jobs=-1, verbose=2)
lr_grid_summary.fit(X_summary_train, y_summary_train)
print(lr_grid_summary.best_params_)
# content
lr_grid_content = GridSearchCV(lr, param_grid=content_grid_param_lr, cv=5,
                               n_jobs=-1, verbose=2)
lr_grid_content.fit(X_content_train, y_content_train)
print(lr_grid_content.best_params_)

"""
Ergebnisse GridSearchCV:
 
title_param_lr= {'penalty': 'l2', 'C': 10}
summary_param_lr={'penalty': 'l2', 'C': 10000}
content_param_lr={'penalty': 'l2', 'C': 10}
"""

###################################################################
####################----RandomForest----###########################
###################################################################

# erstellen eines Hyperparameterraums für RF

random_grid = {'bootstrap': [True, False],
               'max_depth': [10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
               'max_features': ['auto', 'sqrt'],
               'min_samples_leaf': [1, 2, 4],
               'min_samples_split': [2, 5, 10],
               'n_estimators': [200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000]}

# title
rf_random_title = RandomizedSearchCV(estimator=rf, param_distributions=random_grid,
                                     cv=5, n_iter=100, verbose=2, random_state=42, n_jobs=-1)
rf_random_title.fit(X_title_train, y_title_train)
print(rf_random_title.best_params_)

#summary
rf_random_summary = RandomizedSearchCV(estimator=rf, param_distributions=random_grid,
                                       cv=5, n_iter=100, verbose=2, random_state=42, n_jobs=-1)
rf_random_summary.fit(X_summary_train, y_summary_train)
print(rf_random_summary.best_params_)

#content
rf_random_content = RandomizedSearchCV(estimator=rf, param_distributions=random_grid,
                                       cv=5, n_iter=100, verbose=2, random_state=42, n_jobs=-1)
rf_random_content.fit(X_content_train, y_content_train)
print(rf_random_content.best_params_)

"""
RandomizedSearchCV Ergebnisse

title = {'n_estimators': 1800,
 		'min_samples_split': 5,
		'min_samples_leaf': 1,
 		'max_features': 'sqrt',
 		'max_depth': 70, 
		'bootstrap': False}


summary= {'n_estimators': 1200,
 	     'min_samples_split': 2,
	     'min_samples_leaf': 1,
	     'max_features': 'sqrt',
  	     'max_depth': 100,
    	 'bootstrap': False}
    	 
content = {'n_estimators': 1200, 
	       'min_samples_split': 2,
 	       'min_samples_leaf': 1, 
	       'max_features': 'sqrt',
 	       'max_depth': 100,
           'bootstrap': False}    	 
"""


# Neuer Hyperparameterraum RF für 

grid_param_title = {'n_estimators': [1700, 1800, 1900, 2000],
                    'min_samples_split': [3, 5, 7],
                    'min_samples_leaf': [1, 2, 3],
                    'max_features': ["sqrt"],
                    'max_depth': [70, 80, 90, 100],
                    'bootstrap': [False]}
rid_param_summary = {'n_estimators': [1100, 1200, 1300, 1400],
                     'min_samples_split': [2, 4, 6],
                     'min_samples_leaf': [1, 2, 3],
                     'max_features': ['sqrt'],
                     'max_depth': [100, 110, 120, 130],
                     'bootstrap': [False]}
grid_param_content = {'n_estimators': [1100, 1200, 1300, 1400],
                      'min_samples_split': [2, 4, 6],
                      'min_samples_leaf': [1, 3, 5],
                      'max_features': ['sqrt'],
                      'max_depth': [100, 110, 120, 130],
                      'bootstrap': [False]}

# anwenden von GridSearchCV auf RF

# title                      
rf_grid_title = GridSearchCV(estimator=rf, param_grid=grid_param_title,
                                   cv=5, n_jobs=-1, verbose=2)
rf_grid_title.fit(X_title_train, y_title_train)
print(rf_grid_title.best_params_)

# summary
rf_grid_summary = GridSearchCV(estimator=rf, param_grid=grid_param_summary,
                                     cv=5, n_jobs=-1, verbose=2)
rf_grid_summary.fit(X_summary_train, y_summary_train)
print(rf_grid_summary.best_params_)

# content
rf_grid_content = GridSearchCV(estimator=rf, param_grid=grid_param_content,
                                     cv=5, n_jobs=-1, verbose=2)
rf_grid_content.fit(X_content_train, y_content_train)
print(rf_grid_content.best_params_)


"""
Ergebnisse GridSearchCV für RF:


title = {n_estimators=2000, 
         min_samples_split=7,
         min_samples_leaf=1,
         max_features="sqrt",
         max_depth=70, bootstrap=False}

summary = {n_estimators=1300,
           min_samples_split=2,
           min_samples_leaf=1, max_features="sqrt",
           max_depth=100, bootstrap=False}

content = {n_estimators=1100,
           min_samples_split=5,
           min_samples_leaf=1, 
           max_features="sqrt",
           max_depth=100, bootstrap=False)
"""

rf_grid_title = RandomForestClassifier(n_estimators=2000, min_samples_split=7,
                                       min_samples_leaf=1, max_features="sqrt",
                                       max_depth=70, bootstrap=False)

rf_grid_summary = RandomForestClassifier(n_estimators=1300, min_samples_split=2,
                                         min_samples_leaf=1, max_features="sqrt",
                                         max_depth=100, bootstrap=False)

rf_grid_content = RandomForestClassifier(n_estimators=1100, min_samples_split=5,
                                         min_samples_leaf=1, max_features="sqrt",
                                         max_depth=100, bootstrap=False)

lr_grid_title = LogisticRegression(penalty="l2", C=10, solver='liblinear')

lr_grid_summary = LogisticRegression(penalty="l2", C=10000, solver='liblinear')

lr_grid_content = LogisticRegression(penalty="l2", C=10, solver='liblinear')

