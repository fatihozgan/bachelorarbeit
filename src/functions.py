import nltk
from nltk.corpus import stopwords
import pandas as pd
import numpy as np
from porter2stemmer import Porter2Stemmer
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, multilabel_confusion_matrix, confusion_matrix, f1_score
from sklearn.ensemble import VotingClassifier
import itertools

try:
    nltk.data.find('tokenizers\punkt')
except LookupError:
    nltk.download('punkt')

try:
    nltk.data.find('corpora\stopwords')
except LookupError:
    nltk.download('stopwords')
stopWords = set(stopwords.words("english"))

stemmer = Porter2Stemmer()


def preprocess(x):
    'tokenizing --> removing stopwords --> stemming --> join'
    x = [word_tokenize(word)for word in x]
    removestopwords(x)
    x = [[stemmer.stem(token) for token in sentence] for sentence in x]
    x = [" ".join(sentence) for sentence in x]
    return x


def removestopwords(x):
    'removes stopword in words in sentences'
    for sentence in x:
        for w in sentence:
            if w in stopWords:
                sentence.remove(w)
    return x


def binarization(df, columnname, columnvalue):
    '''Binarization(df,columnname, columnvalue) df --> used Dataframe
    columnname --> "category" columnvalue --> if target Value == -> 1 != --> 0'''
    df.loc[df[columnname] == columnvalue, columnname] = 1
    df.loc[df[columnname] != 1, columnname] = 0
    return 0


def readexcelfile(path, sheet):
    ' reeds excel file in path and sheet, parses sheet, fill None with "", sets Dataframe lowercase'
    file = pd.ExcelFile(path)
    df = file.parse(sheet)
    df = df.fillna("")
    # set DataFrame lowercase
    df = df.apply(lambda x: x.astype(str).str.lower())
    return df


def bagofwords(dataframe):
    'uses CountVectorizer() and TfidfTransformer to vectorize and weight input'
    count = CountVectorizer()
    tfidf = TfidfTransformer(use_idf=True, norm='l2', smooth_idf=True)
    np.set_printoptions(precision=2)

    dataframe = tfidf.fit_transform(count.fit_transform(dataframe))
    return dataframe


def data_pipeline(df, columnname):
    'select column from df --> preprocess --> bagofwords'
    column = df.loc[:, columnname]
    column = bagofwords(preprocess(column))
    return column


def modelfit(data_tfidf, label, model_list, model_names, ensemble=None):
    Xtrain, Xtest, ytrain, ytest = train_test_split(data_tfidf, label,
                                                    test_size=0.2, random_state=1)
    accuracy_list = []
    f1score_list = []
    confusion_matrix_list = []
    
    

    if label.ndim > 1:
        conf_matrix = multilabel_confusion_matrix
        average = 'micro'
    else:
        conf_matrix = confusion_matrix
        average = 'binary'

    for clf in model_list:
        clf.fit(Xtrain, ytrain)
        ypred = clf.predict(Xtest)

        confusion_matrix_list.append(conf_matrix(ytest, ypred))
        accuracy_list.append(accuracy_score(ytest, ypred))
        f1score_list.append(f1_score(ytest, ypred, average=average))

    results = dict(zip(model_names, zip(
        accuracy_list, f1score_list, confusion_matrix_list)))

    if ensemble != None:
        voting = VotingClassifier(ensemble, voting="hard")
        voting.fit(Xtrain, ytrain)
        ensemble_score = voting.score(Xtest, ytest)
        results["ensemble_score"] = ensemble_score
    
    return results